package com.example.springlearning.rest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.security.Principal;

@Controller
public class LoginController {

    @GetMapping("/login")
    public String login(Model model, Principal principal, RedirectAttributes flash){

            System.out.println("Iniciando sesión...");
            if(principal != null){
                flash.addFlashAttribute("info", "Ya tiene una sesión iniciada");
                return "redirect:/";
            }


        return "login";
    }

}
