package com.example.springlearning.rest.model.repository;

import com.example.springlearning.rest.model.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Integer> {

    @Query(value="select user from User user where user.user = :user and user.password = :password")
    User login(@Param("user") String user, @Param("password") String password);

    @Query(value="select user from User user where user.id = :idUser")
    User findUserById(@Param("idUser") Integer idUser);
}
