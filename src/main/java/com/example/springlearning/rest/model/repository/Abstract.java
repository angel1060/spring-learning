package com.example.springlearning.rest.model.repository;

import javax.persistence.EntityManager;
import java.sql.SQLException;
import java.util.List;


/**
 *
 * @author avelasquez
 */
public abstract class Abstract<T> {
    private Class<T> entityClass;

    public Abstract(Class<T> entityClass) {
        this.entityClass = entityClass;
    }

    public abstract EntityManager getEntityManager();
    
       
    public void refresh(T entity) {
    	getEntityManager().refresh(entity);
    }
        
    public void save(T entity) throws SQLException{
		getEntityManager().persist(entity);
		getEntityManager().flush();
    }    
    
    public void remove(T entity) {
        getEntityManager().remove(getEntityManager().merge(entity));
    }

    public T find(Object id) {
		//getEntityManager().getEntityManagerFactory().getCache().evictAll();
    	T t = getEntityManager().find(entityClass, id);
    	if(t != null)getEntityManager().refresh(t);
        return t;	
    }
    
    public void update(T entity) {
        getEntityManager().merge(entity);
        getEntityManager().flush();
    }
	
	
    
    @SuppressWarnings({ "rawtypes", "unchecked" })
	public List<T> findAll() {
        javax.persistence.criteria.CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        cq.select(cq.from(entityClass));
        return getEntityManager().createQuery(cq).getResultList();
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
	public List<T> findRange(int[] range) {
        javax.persistence.criteria.CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        cq.select(cq.from(entityClass));
        javax.persistence.Query q = getEntityManager().createQuery(cq);
        q.setMaxResults(range[1] - range[0] + 1);
        q.setFirstResult(range[0]);
        return q.getResultList();
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
    public int count() {
        javax.persistence.criteria.CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        javax.persistence.criteria.Root<T> rt = cq.from(entityClass);
        cq.select(getEntityManager().getCriteriaBuilder().count(rt));
        javax.persistence.Query q = getEntityManager().createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }
}
