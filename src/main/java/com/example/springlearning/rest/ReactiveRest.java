package com.example.springlearning.rest;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class ReactiveRest {

    @GetMapping("/reactiveMethod/{param}")
    public String reactiveMethod(@PathVariable String param){
        return "This is a reactive method...";
    }

}
