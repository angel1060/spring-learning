package com.example.springlearning.rest;

import com.example.springlearning.rest.model.entity.User;
import com.example.springlearning.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping("/findUserById/{idUser}")
    public ResponseEntity<User> findUserById(@PathVariable Integer idUser){
        return new ResponseEntity<>(userService.findUserById(idUser), HttpStatus.OK);
    }
}
