package com.example.springlearning.service;

import com.example.springlearning.rest.model.entity.User;

public interface UserService {
    boolean login(String user, String password);
    User findUserById(Integer idUser);
}
