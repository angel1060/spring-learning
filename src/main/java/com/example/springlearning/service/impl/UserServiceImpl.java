package com.example.springlearning.service.impl;

import com.example.springlearning.rest.model.entity.User;
import com.example.springlearning.rest.model.repository.UserRepository;
import com.example.springlearning.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public boolean login(String user, String password) {
        return false;
    }

    @Override
    public User findUserById(Integer idUser){
        return userRepository.findUserById(idUser);
    }
}
